﻿using System;
using System.Collections.Generic;

namespace UltimaStyleDialogue
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Ultima-Style Dialogue";

            var dialogue = new Dictionary<string, string>()
            {
                ["name"] = "My name is Tom.",
                ["job"] = "I chase Jerry.",
                ["heal"] = "I am hungry!",
                ["jerr"] = "Jerry the mouse!",
                ["hung"] = "I want to eat Jerry!",
                ["bye"] = "Goodbye!",
                ["default"] = "What do you mean?"
            };

            string input = null;
            bool done = false;

            while (!done)
            {
                Console.Write("You say: ");
                input = Console.ReadLine().Trim().ToLowerInvariant();
                if (input.Length > 4)
                    input = input.Substring(0, 4);

                if (input == string.Empty)
                    input = "bye";

                if (input == "bye")
                    done = true;

                if (dialogue.ContainsKey(input))
                    Console.WriteLine(dialogue[input]);
                else
                    Console.WriteLine(dialogue["default"]);
            }
        }
    }
}
