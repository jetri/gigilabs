﻿using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrleansFirstCluster.Interfaces
{
    public interface IUselessGrain : IGrainWithIntegerKey
    {
        Task DoNothingAsync();
    }
}
