﻿using Akka.Actor;
using Akka.DI.Core;
using Akka.DI.Ninject;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetDependencyInjection
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new StandardKernel();
            container.Bind<ILazyAss>().To<SheriffBlake>();

            using (var actorSystem = ActorSystem.Create("MyActorSystem"))
            {
                var resolver = new NinjectDependencyResolver(container, actorSystem);

                var actor = actorSystem.ActorOf(
                    actorSystem.DI().Props<ParentActor>(), "ParentActor");

                Console.WriteLine("Press ENTER to exit...");
                Console.ReadLine();
            }
        }
    }
}
