﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetPersistenceChess.Messages
{
    public class DrawChessBoardMessage
    {
        public char[][] ChessBoard { get; }

        public DrawChessBoardMessage(char[][] chessBoard)
        {
            this.ChessBoard = chessBoard;
        }
    }
}
