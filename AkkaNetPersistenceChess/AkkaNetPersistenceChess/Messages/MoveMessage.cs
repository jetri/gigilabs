﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkkaNetPersistenceChess.Messages
{
    public class MoveMessage
    {
        public string From { get; }
        public string To { get; }

        public MoveMessage(string from, string to)
        {
            this.From = from;
            this.To = to;
        }

        public override string ToString()
        {
            return $"move {this.From} to {this.To}";
        }
    }
}
