﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionBodiedPerson
{
    public class Person
    {
        private string firstName;
        private string lastName;

        public string FirstName => this.firstName;

        public string LastName => this.lastName;

        public string FullName => string.Format("{0} {1}", this.firstName, this.lastName);

        public Person(string firstName, string lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }
    }
}
