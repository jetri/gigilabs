﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionBodiedInventory
{
    class Program
    {
        static void Main(string[] args)
        {
            var inventory = new Inventory();
            inventory.Put(1, "cake");
            inventory.Put(2, "bread");

            Console.WriteLine(inventory[2]);

            Console.ReadLine();
        }
    }
}
