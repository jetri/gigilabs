﻿using Orleans;
using Orleans.Concurrency;
using OrleansStatelessWorkerDashboard.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrleansStatelessWorkerDashboard.Grains
{
    [StatelessWorker]
    public class WorkerGrain : Grain, IWorkerGrain
    {
        private int counter = 0;

        public Task WorkAsync()
        {
            Console.WriteLine($"Working {++counter}!");

            return Task.CompletedTask;
        }
    }
}
