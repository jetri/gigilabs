﻿using Orleans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrleansDemo.Interfaces
{
    public interface IPersonRegistryGrain : IGrainWithStringKey
    {
        Task RegisterGrainAsync(string grainId);
        Task UnregisterGrainAsync(string grainId);
        Task ListRegistryAsync();
    }
}
