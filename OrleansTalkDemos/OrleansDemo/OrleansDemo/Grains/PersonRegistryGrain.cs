﻿using Orleans;
using OrleansDemo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrleansDemo.Grains
{
    public class PersonRegistryGrain : Grain, IPersonRegistryGrain
    {
        private HashSet<string> personRegistry = new HashSet<string>();

        #region IPersonRegistryGrain implementation

        public Task RegisterGrainAsync(string grainId)
        {
            this.personRegistry.Add(grainId);

            Console.WriteLine($"{grainId} has registered.");

            return Task.CompletedTask;
        }

        public Task UnregisterGrainAsync(string grainId)
        {
            this.personRegistry.Remove(grainId);

            Console.WriteLine($"{grainId} has unregistered.");

            return Task.CompletedTask;
        }

        public Task ListRegistryAsync()
        {
            Console.WriteLine("Current registry:");
            foreach (string person in this.personRegistry)
                Console.WriteLine($"  {person}");

            return Task.CompletedTask;
        }

        #endregion IPersonRegistryGrain implementation

        #region Life cycle hooks

        public override Task OnActivateAsync()
        {
            Console.WriteLine("PersonRegistryGrain activated!");

            return base.OnActivateAsync();
        }

        public override Task OnDeactivateAsync()
        {
            Console.WriteLine("PersonRegistryGrain deactivated!");

            return base.OnDeactivateAsync();
        }

        #endregion Life cycle hooks
    }
}
