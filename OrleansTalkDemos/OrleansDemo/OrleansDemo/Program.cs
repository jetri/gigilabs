using System;
using System.Threading.Tasks;

using Orleans;
using Orleans.Runtime.Configuration;
using OrleansDemo.Interfaces;
using System.Threading;

namespace OrleansDemo
{
    /// <summary>
    /// Orleans test silo host
    /// </summary>
    public class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Orleans Demo";

            // The Orleans silo environment is initialized in its own app domain in order to more
            // closely emulate the distributed situation, when the client and the server cannot
            // pass data via shared memory.
            AppDomain hostDomain = AppDomain.CreateDomain("OrleansHost", null, new AppDomainSetup
            {
                AppDomainInitializer = InitSilo,
                AppDomainInitializerArguments = args,
            });

            var config = ClientConfiguration.LocalhostSilo();
            GrainClient.Initialize(config);

            // TODO: once the previous call returns, the silo is up and running.
            //       This is the place your custom logic, for example calling client logic
            //       or initializing an HTTP front end for accepting incoming requests.

            Run();

            Console.WriteLine("Orleans Silo is running.\nPress Enter to terminate...");
            Console.WriteLine();
            Console.ReadLine();

            hostDomain.DoCallBack(ShutdownSilo);
        }

        static async void Run()
        {
            var joe = GrainClient.GrainFactory.GetGrain<IPersonGrain>("Joe");
            await joe.SayHelloAsync();

            Thread.Sleep(5000);
            Console.WriteLine();

            var bob = GrainClient.GrainFactory.GetGrain<IPersonGrain>("Bob");
            await bob.SayHelloAsync();

            Thread.Sleep(5000);
            Console.WriteLine();

            var personRegistry = GrainClient.GrainFactory.GetGrain<IPersonRegistryGrain>("Registry");
            await personRegistry.ListRegistryAsync();

            Thread.Sleep(5000);
            Console.WriteLine();

            await joe.SleepAsync();
            Thread.Sleep(2000);
            await personRegistry.ListRegistryAsync();

            Console.WriteLine();
        }

        static void InitSilo(string[] args)
        {
            hostWrapper = new OrleansHostWrapper(args);

            if (!hostWrapper.Run())
            {
                Console.Error.WriteLine("Failed to initialize Orleans silo");
            }
        }

        static void ShutdownSilo()
        {
            if (hostWrapper != null)
            {
                hostWrapper.Dispose();
                GC.SuppressFinalize(hostWrapper);
            }
        }

        private static OrleansHostWrapper hostWrapper;
    }
}
