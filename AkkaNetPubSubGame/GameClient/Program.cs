﻿using Akka.Actor;
using GameClient.Actors;
using GameClient.Messages;
using GameMessages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var configManager = ConfigurationManager.AppSettings;
            string actorSystemName = configManager["actorSystemName"];
            char avatar = configManager["avatar"][0];

            Console.Title = $"{actorSystemName} - Client";
            Console.OutputEncoding = Encoding.UTF8;
            Console.CursorVisible = false;

            try
            {
                using (var actorSystem = ActorSystem.Create(actorSystemName))
                {
                    short currentX = 40;
                    short currentY = 12;
                    Guid subscriberGuid = Guid.NewGuid();

                    var gameRendererActor = actorSystem.ActorOf(
                        Props.Create<GameRenderingActor>(), "GameRenderingActor");

                    var gameClientActor = actorSystem.ActorOf(
                        Props.Create<GameClientActor>(gameRendererActor,
                            currentX, currentY, subscriberGuid, avatar),
                        "GameClientActor");

                    HandleInput(gameClientActor);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        static void HandleInput(IActorRef gameClientActor)
        {
            while (true)
            {
                var key = Console.ReadKey(intercept: true);

                switch (key.Key)
                {
                    case ConsoleKey.LeftArrow:
                        gameClientActor.Tell(new MoveLeftMessage());
                        break;
                    case ConsoleKey.RightArrow:
                        gameClientActor.Tell(new MoveRightMessage());
                        break;
                    case ConsoleKey.UpArrow:
                        gameClientActor.Tell(new MoveUpMessage());
                        break;
                    case ConsoleKey.DownArrow:
                        gameClientActor.Tell(new MoveDownMessage());
                        break;
                    case ConsoleKey.Escape:
                        gameClientActor.Tell(new QuitGameMessage());
                        return;
                }
            }
        }
    }
}
