﻿using Akka.Actor;
using GameClient.Messages;
using GameMessages;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameClient.Actors
{
    public class GameClientActor : TypedActor,
        IHandle<UpdateLocationMessage>,
        IHandle<UnsubscribeMessage>,
        IHandle<MoveLeftMessage>,
        IHandle<MoveRightMessage>,
        IHandle<MoveUpMessage>,
        IHandle<MoveDownMessage>,
        IHandle<QuitGameMessage>
    {
        private IActorRef gameRenderingActor;
        private ActorSelection remoteServerActor;

        // client actor state
        private short currentX;
        private short currentY;
        private Guid subscriberGuid;
        private char avatar;

        public GameClientActor(IActorRef gameRenderingActor, short initialX,
            short initialY, Guid subscriberGuid, char avatar)
        {
            this.gameRenderingActor = gameRenderingActor;

            string serverActorAddress = ConfigurationManager
                .AppSettings["serverActorAddress"];
            this.remoteServerActor = Context.ActorSelection(serverActorAddress);

            this.currentX = initialX;
            this.currentY = initialY;
            this.subscriberGuid = subscriberGuid;
            this.avatar = avatar;

            this.Subscribe();
        }

        #region Helpers

        private void Subscribe()
        {
            // send subscribe message

            var subscribeMessage = new SubscribeMessage(subscriberGuid);
            remoteServerActor.Tell(subscribeMessage, Self);

            // send initial location

            SendLocationUpdate(0, 0);
        }

        private void SendLocationUpdate(short oldX, short oldY)
        {
            var updateLocationMessage = new UpdateLocationMessage(subscriberGuid,
                avatar, oldX, oldY, currentX, currentY);
            remoteServerActor.Tell(updateLocationMessage);
        }

        private void MoveAndSendLocationUpdate(Action move)
        {
            short oldX = currentX;
            short oldY = currentY;

            move();

            this.SendLocationUpdate(oldX, oldY);
        }

        #endregion Helpers

        #region Message handlers - movement

        public void Handle(MoveLeftMessage message)
        {
            this.MoveAndSendLocationUpdate(() => currentX--);
        }

        public void Handle(MoveRightMessage message)
        {
            this.MoveAndSendLocationUpdate(() => currentX++);
        }

        public void Handle(MoveUpMessage message)
        {
            this.MoveAndSendLocationUpdate(() => currentY--);
        }

        public void Handle(MoveDownMessage message)
        {
            this.MoveAndSendLocationUpdate(() => currentY++);
        }

        public void Handle(QuitGameMessage message)
        {
            short oldX = currentX;
            short oldY = currentY;

            var unsubscribeMessage = new UnsubscribeMessage(subscriberGuid,
                oldX, oldY);
            remoteServerActor.Tell(unsubscribeMessage);
        }

        #endregion Message handlers - movement

        #region Message handlers - messages from server

        public void Handle(UpdateLocationMessage message)
        {
            var eraseMessage = new EraseMessage(message.OldX, message.OldY);
            var drawMessage = new DrawMessage(message.NewX, message.NewY,
                message.Avatar);

            this.gameRenderingActor.Tell(eraseMessage);
            this.gameRenderingActor.Tell(drawMessage);
        }

        public void Handle(UnsubscribeMessage message)
        {
            var eraseMessage = new EraseMessage(message.LastX, message.LastY);

            this.gameRenderingActor.Tell(eraseMessage);
        }

        #endregion Message handlers - messages from server
    }
}
