﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameClient.Messages
{
    public class EraseMessage
    {
        public short X { get; }
        public short Y { get; }

        public EraseMessage(short x, short y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}
